# FAT-STM32

A FAT filesystem implementation for STM32F3Discovery based on OpenFAT project.

## Dependencies

* GNU ARM Embedded Toolchain
* ST-Link

## Install

* Download the repo

$ git clone https://rlmrjmnz@bitbucket.org/rlmrjmnz/fat-stm32.git

* Get the submodules

$ cd fat-stm32
$ git submodule init
$ git submodule update

## Pre-Build

We have to change some flags to create OpenFat static lib for STM32.
Change the lines below

CFLAGS += -mcpu=cortex-m3 -mthumb
LDFLAGS += -mcpu=cortex-m3 -mthumb -march=armv7 -mfix-cortex-m3-ldrd -msoft-float 

as follows

CFLAGS += -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16
LDFLAGS += -mcpu=cortex-m4 -mthumb -march=armv7 -mfix-cortex-m4-ldrd -mfloat-abi=hard -mfpu=fpv4-sp-d16 

and comment the line 

$(MAKE) -C $(HOST)

## Build

* Build the libraries

$ cd lib/libopencm3
$ make

$ cd lib/libopencm3-plus
$ make

$ cd lib/openfat
$ make

* Build the example

$ cd src
$ make

* Flash the STM32

$ cd src
$ make flash
